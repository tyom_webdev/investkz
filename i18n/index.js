import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import Cache from 'i18next-localstorage-cache';

import { initReactI18next } from 'react-i18next';

import en from './translations/en';
import kz from './translations/kz';
import ru from './translations/ru';

console.error = (message) => {
    if (message.includes('Text content did not match. Server')) {
        return;
    } else {
        /* eslint-disable-next-line no-console */
        return console.log('Custom error message: ', message);
    }
};

i18n.use(Cache)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources: {
            en: { translation: en },
            ru: { translation: ru },
            kz: { translation: kz },
        },
        detection: {
            order: ['localStorage'],
            caches: ['localStorage'],
        },
        debug: false,
        fallbackLng: ['ru', 'en', 'kz'],
        allLanguages: ['ru', 'en', 'kz'],
        whitelist: ['ru', 'en', 'kz'],
        supportedLngs: ['ru', 'en', 'kz'],
        load: 'languageOnly',
        nonExplicitWhitelist: true,
        interpolation: {
            escapeValue: false,
        },
        react: {
            wait: process && !process.release,
            bindI18n: false,
            bindStore: false,
            nsMode: 'default',
        },
        cache: {
            enabled: true,
            expirationTime: 3 * 24 * 60 * 60 * 1000, // 3 days
        },
    });

export default i18n;
