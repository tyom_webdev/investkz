import React from 'react';
import { useTranslation } from 'react-i18next';

import AdvantagesHome from '../components/advantages';
import AdvantagesItem from '../components/advantages/item';
import CarouselHome from '../components/carousel-home';
import DescriptionHome from '../components/descreption';
import ExperiansHome from '../components/experians';
import Footer from '../components/footer';
import HeaderHome from '../components/header';
import HowStartHome from '../components/how-start';
import NavMenu from '../uikit/nav';
import SubNav from '../uikit/nav/sub-menu';

export default function Home() {
    const { t } = useTranslation();

    return (
        <div className="home">
            <NavMenu />
            <SubNav />
            <HeaderHome />
            <CarouselHome />
            <AdvantagesHome title={t('platforms_tl')} isOnBorder={true}>
                <AdvantagesItem
                    number={'01'}
                    imgPath={'/img/advantages/fast.png'}
                    title={t('platform1_tl')}
                    desc={t('platform1_desc')}
                />
                <AdvantagesItem
                    number={'02'}
                    imgPath={'/img/advantages/full.png'}
                    title={t('platform2_tl')}
                    desc={t('platform2_desc')}
                />
                <AdvantagesItem
                    number={'03'}
                    imgPath={'/img/advantages/secure.png'}
                    title={t('platform3_tl')}
                    desc={t('platform3_desc')}
                />
            </AdvantagesHome>
            <AdvantagesHome title={t('tokens_tl')} isOnBorder={true}>
                <AdvantagesItem
                    number={'01'}
                    imgPath={'/img/advantages/token-1.png'}
                    title={t('token1_tl')}
                    desc={t('token1_desc')}
                />
                <AdvantagesItem
                    number={'02'}
                    imgPath={'/img/advantages/token-2.png'}
                    title={t('token2_tl')}
                    desc={t('token2_desc')}
                />
                <AdvantagesItem
                    number={'03'}
                    imgPath={'/img/advantages/token-3.png'}
                    title={t('token3_tl')}
                    desc={t('token3_desc')}
                />
            </AdvantagesHome>
            <AdvantagesHome title={t('aifcs_tl')} isOnBorder={false}>
                <AdvantagesItem
                    number={'01'}
                    imgPath={'/img/advantages/mfca-1.png'}
                    title={t('aifc1_tl')}
                    desc={t('aifc1_desc')}
                />
                <AdvantagesItem
                    number={'02'}
                    imgPath={'/img/advantages/mfca-2.png'}
                    title={t('aifc2_tl')}
                    desc={t('aifc2_desc')}
                />
                <AdvantagesItem
                    number={'03'}
                    imgPath={'/img/advantages/mfca-3.png'}
                    title={t('aifc3_tl')}
                    desc={t('aifc3_desc')}
                />
            </AdvantagesHome>
            <ExperiansHome />
            <DescriptionHome />
            <HowStartHome />
            <Footer />
        </div>
    );
}
