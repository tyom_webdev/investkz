import React from 'react';

import Footer from '../components/footer';
import SupportHeader from '../components/support-header';
import TokensTab from '../components/tokens-tab';
import NavMenuUser from '../uikit/navuser';
import SubNavUser from '../uikit/navuser/sub-menu';

export default function Home() {
    return (
        <div className="support">
            <NavMenuUser />
            <SubNavUser />
            <SupportHeader page="tokens" />
            <TokensTab />
            <Footer />
        </div>
    );
}
