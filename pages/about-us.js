import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import CooperationPage from '../components/cooperationPage';
import Footer from '../components/footer';
import NavMenu from '../uikit/nav';
import NavPanel from '../uikit/nav-panel';
import SubNav from '../uikit/nav/sub-menu';

export default function AboutUs() {
    const { t } = useTranslation();

    const [steps] = useState([
        {
            link: '/',
            name: t('home'),
        },
        {
            link: '/about-us',
            name: t('about_company'),
        },
    ]);

    return (
        <div className="home">
            <NavMenu />
            <SubNav />
            <NavPanel steps={steps} />
            <CooperationPage />
            <Footer />
        </div>
    );
}
