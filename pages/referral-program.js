import React from 'react';
import { useTranslation } from 'react-i18next';

import Brendctumbs from '../components/brendcrumbs';
import BrendctumbsItem from '../components/brendcrumbs/item';
import Footer from '../components/footer';
import ReferralProgram from '../components/referral-program';
import NavMenu from '../uikit/nav';
import SubNav from '../uikit/nav/sub-menu';

export default function Faq() {
    const { t } = useTranslation();

    return (
        <div className="faq">
            <NavMenu />
            <SubNav />
            <Brendctumbs>
                <BrendctumbsItem link={<a href='/'>{t('home')}</a>} />
                <BrendctumbsItem text={<p>{t('ref_program')}</p>} />
            </Brendctumbs>
            <ReferralProgram />
            <Footer />
        </div>
    );
}
