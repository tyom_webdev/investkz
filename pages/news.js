import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import Footer from '../components/footer';
import News from '../components/news/News';
import NavMenu from '../uikit/nav';
import NavPanel from '../uikit/nav-panel';
import SubNav from '../uikit/nav/sub-menu';

export default function Home() {
    const { t } = useTranslation();

    const [steps] = useState([
        {
            link: '/',
            name: t('home'),
        },
        {
            link: '/news',
            name: t('news'),
        },
    ]);

    return (
        <div className="support">
            <NavMenu />
            <SubNav />
            <NavPanel steps={steps} />
            <News />
            <Footer />
        </div>
    );
}
