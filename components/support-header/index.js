import { Dropdown, Menu } from 'antd';

import React from 'react';
import { useTranslation } from 'react-i18next';

import Tarifmodal from '/components/tarifmodal';

const SupportHeader = ({ page }) => {
    const { t } = useTranslation();

    const getCurrentPage = (n) => {
        if (n === page) {
            return { display: 'block' };
        }
    };

    const menu = (
        <Menu>
            <Menu.Item>
                <a href="usertokens">{t('tokens')}</a>
            </Menu.Item>
            <Menu.Item>
                <a href="wallet">{t('wallet')}</a>
            </Menu.Item>
            <Menu.Item>
                <a href="anket">{t('anketa')}</a>
            </Menu.Item>
            <Menu.Item>
                <a href="ref">{t('referral')}</a>
            </Menu.Item>
            <Menu.Item>
                <a href="tokens">{t('usertokens')}</a>
            </Menu.Item>
            <Menu.Item>
                <a href="rinok">{t('market')}</a>
            </Menu.Item>
            <Menu.Item>
                <a href="usermenu">{t('help')}</a>
            </Menu.Item>
        </Menu>
    );

    return (
        <div className="support-heder">
            <Tarifmodal />
            <div className="support-heder-cont">
                <div
                    style={getCurrentPage('tokens')}
                    className={`support-heder-cont-item ${page === 'tokens' ? 'support-heder-cont-item-active' : ''}`}
                >
                    <img src="/img/tokens.png" alt="" />
                    <a href="usertokens">{t('tokens')}</a>
                </div>
                <div
                    style={getCurrentPage('wallet')}
                    className={`support-heder-cont-item ${page === 'wallet' ? 'support-heder-cont-item-active' : ''}`}
                >
                    <img src="/img/koshel.png" alt="" />
                    <a href="wallet">{t('wallet')}</a>
                </div>
                <div
                    style={getCurrentPage('anketa')}
                    className={`support-heder-cont-item ${page === 'anketa' ? 'support-heder-cont-item-active' : ''}`}
                >
                    <img src="/img/anketa.png" alt="" />
                    <a href="anket">{t('anketa')}</a>
                </div>
                <div
                    style={getCurrentPage('ref')}
                    className={`support-heder-cont-item ${page === 'ref' ? 'support-heder-cont-item-active' : ''}`}
                >
                    <img src="/img/anketa.png" alt="" />
                    <a href="ref">{t('referral')}</a>
                </div>
                <div
                    style={getCurrentPage('pogashenie')}
                    className={`support-heder-cont-item ${
                        page === 'pogashenie' ? 'support-heder-cont-item-active' : ''
                    }`}
                >
                    <img src="/img/dosroch.png" alt="" />
                    <a href="tokens">{t('usertokens')}</a>
                </div>
                <div
                    style={getCurrentPage('rinok')}
                    className={`support-heder-cont-item ${page === 'rinok' ? 'support-heder-cont-item-active' : ''}`}
                >
                    <img src="/img/rinok.png" alt="" />
                    <a href="rinok">{t('market')}</a>
                </div>
                <div
                    style={getCurrentPage('help')}
                    className={`support-heder-cont-item ${page === 'help' ? 'support-heder-cont-item-active' : ''}`}
                >
                    <img src="/img/help.png" alt="" />
                    <a href="usermenu">{t('help')}</a>
                </div>
            </div>
            <Dropdown overlay={menu} trigger="click" placement="bottomRight">
                <button type="button" className="toggle-submenu">
                    <img src="/img/arrow-select.png" alt="" draggable="false" />
                </button>
            </Dropdown>
        </div>
    );
};

export default SupportHeader;
