import React from 'react';
import { useTranslation } from 'react-i18next';

const Terms = () => {
    const { t } = useTranslation();

    return (
        <div className="terms">
            <div className="container">
                <p className="terms__heading">{t('terms_cop')}</p>
                <p className="terms__subheading">{t('terms_tl1')}</p>
                <p className="terms__texts">
                    {t('terms_tx1')}
                </p>
                <p className="terms__texts">
                    {t('terms_tx2')}
                </p>
                <p className="terms__subheading">{t('terms_tl2')}</p>
                <p className="terms__texts">
                    {t('terms_tx3')}
                </p>
            </div>
        </div>
    );
};

export default Terms;
