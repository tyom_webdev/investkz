import { Menu } from 'antd';
import Dropdown from 'antd/lib/dropdown';

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

const currencies = [
    { v: 'euro_sc', display: 'EUR.sc' },
    { v: 'kzt_sc', display: 'KZT.sc' },
    { v: 'rub_sc', display: 'RUB.sc' },
    { v: 'usd_sc', display: 'USD.sc' },
];

function cloustokenmodalmodal() {
    document.getElementById('tokenmodalpol').style.display = 'none';
}

const Tokenmodalpol = () => {
    const [total, setTotal] = useState('');
    const [currency, setCurrency] = useState({
        v: currencies[0].v,
        display: currencies[0].display,
    });

    const { t } = useTranslation();

    const cards = [
        { v: 'euro_sc', display: t('bank_card') + ' БелВЭБ' },
        { v: 'kzt_sc', display: t('bank_card_other') },
        { v: 'rub_sc', display: t('count_now') },
    ];

    const [card, setCard] = useState({
        v: cards[0].v,
        display: cards[0].display,
    });

    const handleChange = (e) => {
        setTotal(e.target.value);
    };

    const menuCurrency = (
        <Menu>
            {currencies.map((item, index) => (
                <Menu.Item key={index} onClick={() => setCurrency({ v: item.v, display: item.display })}>
                    <span>{item.display}</span>
                </Menu.Item>
            ))}
        </Menu>
    );

    const menuCard = (
        <Menu>
            {cards.map((item, index) => (
                <Menu.Item key={index} onClick={() => setCard({ v: item.v, display: item.display })}>
                    <span>{item.display}</span>
                </Menu.Item>
            ))}
        </Menu>
    );

    return (
        <div className="tokenmodal" id="tokenmodalpol">
            <div className="tokenmodal-cont">
                <button onClick={cloustokenmodalmodal} className="clousmodal"></button>
                <div className="tokenmodal-cont-title">
                    <div className="tokenmodal-cont-title-box">
                        <h3>{t('fill_balance_title')}</h3>
                        <p>{t('withdraw_message')}</p>
                    </div>
                </div>
                <div className="tokenmodal-cont-form">
                    <form action='https://api.digital-investor.kz/api/fill-user-wallet' method="POST" className="tokenmodal-cont-form-box">
                        <div className="tokenmodal-cont-form-box-top">
                            <label htmlFor="TEST1234556879">
                                {t('select_currency')}
                                <Dropdown overlay={menuCurrency}>
                                    <div>
                                        <span>{currency.display}</span>
                                        <img src="/img/arrow-select.png" alt="" />
                                    </div>
                                </Dropdown>
                            </label>
                            <label htmlFor="TESTKBVGJLGV">
                                {t('select_type_fill')}
                                <Dropdown overlay={menuCard}>
                                    <div>
                                        <span>{card.display}</span>
                                        <img src="/img/arrow-select.png" alt="" />
                                    </div>
                                </Dropdown>
                            </label>
                        </div>
                        <div className="tokenmodal-cont-form-box-bottom">
                            <div className="tokenmodal-cont-form-box-bottom-left">
                                <label>
                                    {t('count')}
                                    <input type="text" onChange={handleChange} placeholder="10" required />
                                </label>
                                <label>
                                    {t('total')}
                                    <input
                                        type="text"
                                        readOnly
                                        placeholder="10"
                                        value={total ? total + ' ' + currency.display.replace('.sc', '') : ''}
                                    />
                                </label>
                            </div>
                            <input name='action' value="increase" type='hidden'/>
                            <input name={currency.v} value={total} type='hidden'/>
                            <div className="tokenmodal-cont-form-box-bottom-right">
                                <label>
                                    <input
                                        type="submit"
                                        style={{ cursor: 'pointer' }}
                                        value={t('fill_balance_button')}
                                    />
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Tokenmodalpol;
