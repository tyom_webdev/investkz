import React from 'react';
import { useTranslation } from 'react-i18next';

import Button from '../../uikit/button';

const ReferralProgram = () => {
    const { t } = useTranslation();

    return (
        <div className="referral-program">
            <div className="container">
                <h2 className="title">{t('ref_program')}</h2>
                <p>{t('ref_desc')}</p>
                <div className="img">
                    <img src="/img/ReferralProgram.png" alt=" " draggable="false" />
                </div>
                <div className="baner">
                    <img src="/img/baner.png" alt=" " draggable="false" />
                    <div className="banner-divs">
                        <div>
                            <h2>{t('ref_sec1')}</h2>
                            <p>{t('ref_sec2')}</p>
                        </div>
                        <Button color="submit">{t('ref_btn')}</Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ReferralProgram;
