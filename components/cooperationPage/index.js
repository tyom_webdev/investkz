import { v4 as uuid } from 'uuid';

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import CollapseItem from '../../uikit/collapse';
import Info from '../../uikit/info';
import User from '../../uikit/user';
import CooperationCarousel from '../cooperationCarousel';

const arrayTo2dArray = (array, n) => {
    const result = [];
    for (let i = 0; i < Math.ceil(array.length / n); i++) {
        result[i] = [];
        for (let j = 0; j < n; j++) {
            if (array[i * n + j]) {
                result[i][j] = array[i * n + j];
            }
        }
    }
    return result;
};

const CooperationPage = () => {
    const [personData, setPersonData] = useState([]);
    const [vacancies, setVacancies] = useState([]);
    const [docs, setDocs] = useState([]);

    const { t } = useTranslation();

    useEffect(() => {
        fetch('https://api.digital-investor.kz/api/company-page-info')
            .then((response) => response.json())
            .then((json) => {
                setDocs(json.data.law_documentations);
                setVacancies(json.data.vacancies);
                setPersonData(arrayTo2dArray(json.data.personal, 4));
            });
    }, []);

    const cards = [
        {
            image: 'img/sliderItem1.png',
            name: 'Энэка',
        },
        {
            image: 'img/sliderItem2.png',
            name: 'Хлебозавод № 10',
        },
        {
            image: 'img/sliderItem3.png',
            name: 'Открытая Линия ',
        },
        {
            image: 'img/sliderItem4.png',
            name: 'ГлавИндустрияСтрой',
        },
        {
            image: 'img/sliderItem1.png',
            name: 'Энэка',
        },
        {
            image: 'img/sliderItem2.png',
            name: 'Хлебозавод № 10',
        },
        {
            image: 'img/sliderItem3.png',
            name: 'Открытая Линия ',
        },
        {
            image: 'img/sliderItem4.png',
            name: 'ГлавИндустрияСтрой',
        },
    ];

    return (
        <div className="cooperationPage">
            <div className="container">
                <div className="company">
                    <h1>{t('about_company')}</h1>
                    <p>{t('about_desc')}</p>
                </div>
                <div className="infoBlock">
                    <div className="infoBlock-item">
                        <Info year="2017" month={t('august')} text={t('aug_text')} />
                        <Info year="2017" month={t('december')} text={t('dec_text')} />
                    </div>
                    <div className="infoBlock-item">
                        <Info year="2018" month={t('may')} text={t('may_text')} />
                        <Info year="2020" month={t('april')} text={t('apr_text')} />
                    </div>
                </div>
                <div className="ourTeam">
                    <h2 className="title">{t('our_team')}</h2>
                    {personData &&
                        personData.map((personRow) => {
                            return (
                                <div key={uuid()} className="ourTeam-row">
                                    {personRow.map((person) => {
                                        return (
                                            <User
                                                key={uuid()}
                                                img={person.avatar}
                                                name={`${person.name} ${person.surname}`}
                                                profession={person.position}
                                            />
                                        );
                                    })}
                                </div>
                            );
                        })}
                </div>
                <div className="vacansia">
                    <h2 className="title">{t('vacancies')}</h2>
                    {vacancies &&
                        vacancies.map((item, index) => (
                            <CollapseItem key={index} question={item?.title} answer={item?.description} />
                        ))}
                </div>
            </div>
            <div className="carousel">
                <div className="container">
                    <h2 className="title">{t('our_partners')}</h2>
                    <CooperationCarousel data={cards} />
                </div>
            </div>
            <div className="container">
                <div className="lawDoc" style={{ paddingBottom: '25px' }}>
                    <h2 className="title">{t('about_docs')}</h2>
                    {docs &&
                        docs.map((item, index) => (
                            <CollapseItem key={index} question={item?.title} answer={item?.description} />
                        ))}
                </div>
            </div>
        </div>
    );
};
export default CooperationPage;
