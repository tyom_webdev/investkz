import React from 'react';
import { useTranslation } from 'react-i18next';

const SupportPage = () => {
    const { t } = useTranslation();

    return (
        <div className="support-page">
            <div className="support-page-cont">
                <h1>{t('help_title')}</h1>
                <div className="support-page-cont_box" id="support-page">
                    <h4>{t('help_subtitle')}</h4>
                    <p>{t('help_description')}</p>
                    <div className="support-page-cont_box_flex">
                        <div className="support-page-cont_box_flex_item">
                            <div className="support-page-cont_box_flex_item_img">
                                <img src="/img/phone_icon.svg" alt="" draggable="false" />
                            </div>
                            <div className="support-page-cont_box_flex_item_text">
                                <span>
                                    +7 777 022 66 74
                                    <br />
                                    +7 705 148 29 15
                                </span>
                                <a href="tel: +7 (999) 525-25-57">{t('call')}</a>
                            </div>
                        </div>
                        <div className="support-page-cont_box_flex_item">
                            <div className="support-page-cont_box_flex_item_img">
                                <img src="/img/mail_icon.svg" alt="" draggable="false" />
                            </div>
                            <div className="support-page-cont_box_flex_item_text">
                                <span>info@digital-investor.kz</span>
                                <a href="mailito: mail@kzinvestmarket.com">{t('message')}</a>
                            </div>
                        </div>
                        <div className="support-page-cont_box_flex_item">
                            <div className="support-page-cont_box_flex_item_img">
                                <img src="/img/telegram_icon.svg" alt="" draggable="false" />
                            </div>
                            <div className="support-page-cont_box_flex_item_text">
                                <span>@digitalinvest</span>
                                <a href="/">{t('ask')}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SupportPage;
