import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken } from '../../utils/auth';

function zayavmodalordernone() {
    document.getElementById('winmodal').style.display = 'none';
}

const Winmodal = ({ tokenId }) => {
    const [number, setNumber] = useState(1);
    const [tokenData, setTokenData] = useState();
    const [message, setMessage] = useState({
        type: null,
        msg: '',
    });

    const { t } = useTranslation();

    useEffect(() => {
        tokenId &&
            fetch('https://api.digital-investor.kz/api/get-token-info', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    token_id: tokenId,
                }),
            })
                .then((data) => data.json())
                .then((data) => {
                    setTokenData(data);
                })
                .catch((err) => console.error(err));
    }, [tokenId]);

    const buy = (e) => {
        e.preventDefault();
        if (number < 1) return;
        fetch('https://api.digital-investor.kz/api/buy-tokens', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Bearer ' + getToken(),
            },
            body: JSON.stringify({
                token_id: tokenId,
                tokens_count: number,
            }),
        })
            .then((data) => data.json())
            .then((data) => {
                if (data.status === 200) {
                    setMessage({
                        type: 'success',
                        msg: data.message,
                    });
                    window.location.reload();
                } else {
                    setMessage({
                        type: 'error',
                        msg: data.message,
                    });
                }
            })
            .catch((err) => setMessage(err.message));
    };

    return (
        <div className="zayavmodalorder" id="winmodal">
            <div className="zayavmodalorder-cont">
                <button onClick={zayavmodalordernone} className="clousmodal" />
                <div className="zayavmodalorder-cont-top zayavmodalorder-cont-top-flex">
                    <img src={tokenData ? tokenData.data.company_logo : '/img/nailsimg.png'} alt="" />
                    <div className="zayavmodalorder-cont-top-flex-text">
                        <h3>{tokenData ? tokenData.data.token_name : 'loading'}</h3>
                        <div className="zayavmodalorder-cont-top-box">
                            <div className="zayavmodalorder-cont-top-box-item">
                                <p>{t('token')}</p>
                                <span>{tokenData ? tokenData.data.token_name : 'loading'}</span>
                            </div>
                            <div className="zayavmodalorder-cont-top-box-item">
                                <p>{t('currency')}</p>
                                {tokenData ? <span>{tokenData.data.currency}.sc</span> : 'loading'}
                            </div>
                            <div className="zayavmodalorder-cont-top-box-item">
                                <p>{t('date_percent')}</p>
                                {tokenData ? <span>{tokenData?.data?.percent}%</span> : 'loading'}
                            </div>
                            <div className="zayavmodalorder-cont-top-box-item">
                                <p>{t('price_one_token')}</p>

                                {tokenData ? (
                                    <span>
                                        {tokenData?.data?.toke_cost} {tokenData?.data?.currency}.sc
                                    </span>
                                ) : (
                                    'loading'
                                )}
                            </div>
                            <div className="zayavmodalorder-cont-top-box-item">
                                <p>{t('term_of_circulation')}</p>
                                <span>
                                    {tokenData?.data?.term_of_circulation} {t('days')}
                                </span>
                            </div>
                        </div>
                        <div className="zayavmodalorder-cont-top-flex-text-bottom">
                            <p>
                                {t('tokens_count')}: <br />
                                {t('type_tokens_count')}
                            </p>
                            <div className="zayavmodalorder-cont-top-flex-text-bottom-right">
                                <p>
                                    <span>{tokenData ? tokenData.data.tokens_count : 'loading...'} /</span>
                                    <span>{tokenData ? tokenData.data.sold_tokens : 'loading...'} /</span>
                                    <span>{tokenData ? tokenData.data.available_tokens : 'loading...'}</span>
                                </p>
                                <div className="progres">
                                    <span />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="zayavmodalorder-cont-bottom">
                    <form>
                        <div className="zayavmodalorder-cont-bottom-flex">
                            <label className="inptext">
                                {t('payment_frequency')}
                                <input
                                    disabled
                                    type="text"
                                    placeholder={tokenData ? tokenData.data.payment_frequency : 'loading...'}
                                />
                            </label>
                            <label className="inptext">
                                {t('first_payment_date')}
                                <input
                                    disabled
                                    type="text"
                                    placeholder={tokenData ? tokenData.data.first_payment_date : 'loading...'}
                                />
                            </label>
                            <label className="inptext">
                                {t('circulation_start')}
                                <input
                                    disabled
                                    type="date"
                                    value={tokenData ? tokenData.data.circulation_start.split(' ')[0] : 'loading...'}
                                />
                            </label>
                            <label className="inptext">
                                {t('circulation_end')}
                                <input
                                    disabled
                                    type="date"
                                    value={tokenData ? tokenData.data.circulation_end.split(' ')[0] : 'loading...'}
                                />
                            </label>
                            <label className="inptext">
                                {t('sales_end')}
                                <input
                                    disabled
                                    type="date"
                                    value={tokenData ? tokenData.data.sales_end.split(' ')[0] : 'loading...'}
                                />
                            </label>
                            <label className="inptext">
                                {t('circulation_token')}
                                <input
                                    disabled
                                    type="text"
                                    placeholder={
                                        tokenData
                                            ? `${t('after')} ${tokenData.data.term_of_circulation} ${t('days')}`
                                            : 'loading...'
                                    }
                                />
                            </label>
                            <label className="inptext">
                                {t('number')}
                                <input onChange={(e) => setNumber(e.target.value)} type="number" placeholder="1" />
                            </label>
                        </div>
                        <label className="inpbtn">
                            <input onClick={buy} type="submit" value={t('buy')} />
                            <span
                                style={{
                                    paddingLeft: '20px',
                                    color: message.type === 'error' ? 'red' : '#4BB496',
                                }}
                            >
                                {message.msg}
                            </span>
                        </label>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Winmodal;
