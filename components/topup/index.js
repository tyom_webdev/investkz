import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken, logout } from '../../utils/auth';

function cloustopupmodal() {
    document.getElementById('topup').style.display = 'none';
}

const Topup = ({ allwalletTotal, freebalanceTotal, blockedTotal, currency }) => {
    const [amount, setAmount] = useState();
    const [message, setMessage] = useState();

    const { t } = useTranslation();

    const [commission] = useState([0.36, 0.57, 0.25]);
    const [finalPrice, setFinalPrice] = useState();
    const [selectedId, setSelectedId] = useState();

    useEffect(() => {
        if (!selectedId || !amount) {
            return;
        }
        const index = +selectedId.replace('in', '') - 1;
        let result = amount * (commission[index] / 100);
        let finalResult = amount - result;
        setFinalPrice(finalResult);
    }, [amount, selectedId, commission]);

    const onAmountChange = (e) => {
        const { value } = e.target;
        setAmount(value);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        const postParams = {
            action: 'increase',
            kzt_sc: 0,
            rub_sc: 0,
            euro_sc: 0,
            usd_sc: 0,
        };
        if (currency.toLowerCase() === 'eur') {
            postParams['euro_sc'] = +amount;
        } else {
            postParams[currency.toLowerCase() + '_sc'] = +amount;
        }
        fetch('https://api.digital-investor.kz/api/check-user-anket', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Bearer ' + getToken(),
            },
        })
            .then((res) => res.json())
            .then(({ data }) => {
                if (!data.has_anket) {
                    setMessage(t('empty_anket'));
                } else {
                    fetch('https://api.digital-investor.kz/api/fill-user-wallet', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Bearer ' + getToken(),
                        },
                        body: JSON.stringify(postParams),
                    }).then((res) => {
                        if (res.status === 401) {
                            logout();
                        }
                        window.location.reload();
                        return res.json();
                    });
                }
            });
    };

    function changeInput(id) {
        setSelectedId(id);
    }

    return (
        <div className="topup" id="topup">
            <div className="topup-cont">
                <button onClick={cloustopupmodal} className="clousmodal"></button>
                <div className="topup-cont-info">
                    <h3>{currency}.sc</h3>
                    <div className="topup-cont-info-item">
                        <p>{t('wallet_balance')}</p>
                        <span>{allwalletTotal}</span>
                    </div>
                    <div className="topup-cont-info-item">
                        <p>{t('free_balance')}</p>
                        <span>{freebalanceTotal}</span>
                    </div>
                    <div className="topup-cont-info-item">
                        <p>{t('blocked_balance')}</p>
                        <span>{blockedTotal}</span>
                    </div>
                </div>
                <div className="topup-cont-form">
                    <form onSubmit={onSubmit}>
                        <div className="topup-cont-form-top">
                            <p>{t('input_count_price')}</p>
                            <label>
                                <input onChange={onAmountChange} type="number" placeholder="1 200" />
                            </label>
                        </div>
                        <div className="topup-cont-form-chek" onChange={(e) => changeInput(e.target.id)}>
                            <p>{t('input_type_for_fill')}</p>
                            <label htmlFor="in1">
                                <input id="in1" type="radio" name="radio" />
                                <span className="chekspan"></span>
                                <p>
                                    {t('bank_card')} БелВЭБ{' '}
                                    <span>
                                        {t('commission')} {commission[0]}%
                                    </span>
                                </p>
                            </label>
                            <label htmlFor="in2">
                                <input id="in2" type="radio" name="radio" />
                                <span className="chekspan"></span>
                                <p>
                                    {t('bank_card_other')}{' '}
                                    <span>
                                        {t('commission')} {commission[1]}%
                                    </span>
                                </p>
                            </label>
                            <label htmlFor="in3">
                                <input id="in3" type="radio" name="radio" />
                                <span className="chekspan"></span>
                                <p>
                                    {t('count_now')}{' '}
                                    <span>
                                        {t('commission')} {commission[2]}%
                                    </span>
                                </p>
                            </label>
                        </div>
                        {finalPrice ? (
                            <p className="itogprice">
                                {t('final_commission')}: {finalPrice}
                            </p>
                        ) : null}
                        <label className="topupsend">
                            <input type="submit" value={t('fill')} />
                            <span
                                style={{
                                    paddingLeft: '20px',
                                    color: 'red',
                                }}
                            >
                                {message}
                            </span>
                        </label>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Topup;
