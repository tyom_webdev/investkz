import { Pagination } from 'antd';

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken } from '../../utils/auth';
import DateModal from '../wallet/modals/DateModal';
import FilterModal from '../wallet/modals/FilterModal';
import Wallettableblock from './item/index';

const PAGE_SIZE = 20;

const Wallettable2 = ({ title, link }) => {
    const [historyData, setHistoryData] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalCount, setTotalCount] = useState(null);
    const [isFiltersOpen, setIsFiltersOpen] = useState(false);
    const [isDatesOpen, setIsDatesOpen] = useState(false);
    const [filters, setFilters] = useState({
        by_currency: '',
        by_operation: '',
    });
    const [dates, setDates] = useState({
        date_from: '',
        date_to: '',
    });

    const { t } = useTranslation();

    useEffect(() => {
        fetch(
            `https://api.digital-investor.kz/api/get-wallet-history/?page=${currentPage}&per_page=${PAGE_SIZE}&by_currency=${filters.by_currency}&by_operation=${filters.by_operation}&date_from=${dates.date_from}&date_to=${dates.date_to}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getToken(),
                },
            }
        )
            .then((res) => res.json())
            .then(({ data }) => {
                setTotalCount(data.total);
                setHistoryData(data.data);
            });
    }, [currentPage, dates, filters]);

    const handleChange = (e) => {
        setCurrentPage(e);
    };

    return (
        <>
            {!!isFiltersOpen && <FilterModal close={() => setIsFiltersOpen(false)} setFilters={setFilters} />}
            {!!isDatesOpen && <DateModal close={() => setIsDatesOpen(false)} setDates={setDates} />}
            <div className="tokens-tab">
                <div className="tokens-tab-cont">
                    <div className="tokens-tab-cont-title">
                        <div className="tokens-tab-cont-title-left">
                            <h1>{title}</h1>
                            <a href="tarif">{link}</a>
                        </div>
                        <div className="tokens-tab-cont-title-right tokens-tab-cont-title-rightmin">
                            <button
                                className="tokens-tab-cont-title-right-item"
                                style={{ cursor: 'pointer' }}
                                onClick={() => setIsFiltersOpen(true)}
                            >
                                <img src="/img/sacacsasc.png" alt="" draggable="false" />
                                <p>{t('show_filters')}</p>
                            </button>
                            <button
                                className="tokens-tab-cont-title-right-item"
                                style={{ cursor: 'pointer' }}
                                onClick={() => setIsDatesOpen(true)}
                            >
                                <img src="/img/asdasd.png" alt="" draggable="false" />
                                <p>{t('select_dating')}</p>
                            </button>
                        </div>
                    </div>
                    <Wallettableblock data={historyData} />
                    {!!(totalCount > PAGE_SIZE) && (
                        <Pagination
                            onChange={handleChange}
                            current={currentPage}
                            pageSize={PAGE_SIZE}
                            defaultCurrent={1}
                            total={totalCount}
                        />
                    )}
                </div>
            </div>
        </>
    );
};

export default Wallettable2;
