import { PDFDownloadLink } from '@react-pdf/renderer';

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import Doc from '../../../wallet/Doc';

function Wallettableitem2({ row, currencies }) {
    const [isLoaded, setIsLoaded] = useState(false);
    const { t } = useTranslation();

    useEffect(() => setIsLoaded(true), []);

    return (
        <tr className="tabitemtr walletitemtr walletitemtr4">
            <td>
                <p>{row?.operation_type_text}</p>
            </td>
            <td>
                <p>{row?.amount}</p>
            </td>
            <td>
                <p>{currencies[row?.currency]}</p>
            </td>
            <td>
                <p>{row?.operation_date}</p>
            </td>
            <td className="lasttdwallet">
                <p />
                {isLoaded && (
                    <PDFDownloadLink
                        document={<Doc data={row} currencies={currencies} />}
                        fileName={row?.operation_type_text + ' ' + row?.operation_date?.split(' ')[0] + '.pdf'}
                    >
                        {t('get_pdf')}
                    </PDFDownloadLink>
                )}
            </td>
        </tr>
    );
}

export default Wallettableitem2;
