import { v4 as uuidv4 } from 'uuid';

import React from 'react';
import { useTranslation } from 'react-i18next';

import Wallettableitem from './tabe-item';
import WallettableTitle from './tabe-title/index';
import WallettableTitletd from './tabe-title/tabtitletd';

const Wallettableblock = ({ data }) => {
    const { t } = useTranslation();

    const currencies = {
        euro_sc: 'EUR.sc',
        usd_sc: 'USD.sc',
        ktz_sc: 'KTZ.sc',
        rub_sc: 'RUB.sc',
    };

    return (
        <div className="tokens-tab-cont-table">
            <table>
                <tbody>
                    <WallettableTitle>
                        <WallettableTitletd tdtext={t('operation_type')} />
                        <WallettableTitletd tdtext={t('count')} />
                        <WallettableTitletd tdtext={t('currency')} />
                        <WallettableTitletd tdtext={t('date_time')} />
                        <WallettableTitletd tdtext={''} />
                        <WallettableTitletd tdtext={''} />
                    </WallettableTitle>
                    {data && data.map((row) => <Wallettableitem key={uuidv4()} row={row} currencies={currencies} />)}
                </tbody>
            </table>
        </div>
    );
};

export default Wallettableblock;
