import PropTypes from 'prop-types';

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken } from '../../../utils/auth';

function SubmitEvent({ id, close, type }) {
    const [url, setUrl] = useState(null);
    const [info, setInfo] = useState({
        title: '',
        description: '',
    });
    const [message, setMessage] = useState('');

    const { t } = useTranslation();

    useEffect(() => {
        if (type === 'sell') {
            setUrl('https://api.digital-investor.kz/api/secondary/sell-token');
            setInfo({
                title: t('submit_event_1'),
                description: t('submit_event_2'),
            });
        } else {
            setUrl('https://api.digital-investor.kz/api/secondary/buy-token');
            setInfo({
                title: t('submit_event_3'),
                description: t('submit_event_4'),
            });
        }
    }, [type, t]);

    const handleClick = () => {
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getToken(),
                Accept: 'application/json',
            },
            body: JSON.stringify({
                request_id: id,
            }),
        })
            .then((res) => res.json())
            .then((res) => {
                console.info(res);
                if (res.status === 200) {
                    window.location.reload();
                } else {
                    setMessage(res.message);
                }
            })
            .catch((err) => console.info(err));
    };

    return (
        <div className="deletemodal" style={{ display: 'block' }}>
            <div className="deletemodal-cont">
                <button onClick={close} className="clousmodal"></button>
                <div className="deletemodal-cont-top">
                    <h3>{info.title}</h3>
                    <p>{info.description}</p>
                    {message && (
                        <p
                            style={{
                                color: 'red',
                                fontWeight: 600,
                            }}
                        >
                            {message}
                        </p>
                    )}
                </div>
                <div className="deletemodal-cont-bottom">
                    <div className="deletemodal-cont-bottom-box">
                        <button type="button" onClick={handleClick}>
                            {t('delete_3')}
                        </button>
                        <button type="button" onClick={close}>
                            {t('delete_4')}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

SubmitEvent.propTypes = {
    id: PropTypes.number.isRequired,
    close: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
};

export default SubmitEvent;
