import PropTypes from 'prop-types';

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken } from '../../../utils/auth';

function Details({ id, close }) {
    const [data, setData] = useState([]);

    const { t } = useTranslation();

    useEffect(() => {
        fetch('https://api.digital-investor.kz/api/secondary/get-token-info', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getToken(),
                Accept: 'application/json',
            },
            body: JSON.stringify({
                token_id: id,
            }),
        })
            .then((res) => res.json())
            .then(({ data }) => {
                setData(data);
            })
            .catch((err) => console.info(err));
    }, [id]);

    return (
        <div className="zayavmodalorder" style={{ display: 'block' }}>
            <div className="zayavmodalorder-cont">
                <button onClick={close} className="clousmodal"></button>
                <div className="zayavmodalorder-cont-top">
                    <h3>{t('details_1')}</h3>
                    <div className="zayavmodalorder-cont-top-box">
                        <div className="zayavmodalorder-cont-top-box-item">
                            <p>{t('details_2')}</p>
                            <span>
                                {data?.token_cost} {data?.currency}
                            </span>
                        </div>
                        <div className="zayavmodalorder-cont-top-box-item">
                            <p>{t('details_3')}</p>
                            <span>
                                {data?.token_cost * data?.tokens_count} {data?.currency}
                            </span>
                        </div>
                        <div className="zayavmodalorder-cont-top-box-item">
                            <p>{t('details_4')}</p>
                            <span>0 {data?.currency}</span>
                        </div>
                        <div className="zayavmodalorder-cont-top-box-item">
                            <p>{t('details_5')}</p>
                            <span>
                                {data?.token_cost * data?.tokens_count} {data?.currency}
                            </span>
                        </div>
                    </div>
                </div>
                <div className="zayavmodalorder-cont-bottom">
                    <form>
                        <div className="zayavmodalorder-cont-bottom-flex">
                            <label className="inptext">
                            {t('details_6')}
                                <input type="text" placeholder={data?.payment_frequency} readOnly disabled />
                            </label>
                            <label className="inptext">
                            {t('details_7')}
                                <input
                                    type="text"
                                    placeholder={
                                        data?.circulation_start?.split(' ')[0] +
                                        ' - ' +
                                        data?.circulation_end?.split(' ')[0]
                                    }
                                    readOnly
                                    disabled
                                />
                            </label>
                            <label className="inptext">
                            {t('details_8')}
                                <input type="text" placeholder={data?.tokens_count} readOnly disabled />
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

Details.propTypes = {
    id: PropTypes.number.isRequired,
    close: PropTypes.func.isRequired,
};

export default Details;
