import { Pagination } from 'antd';

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken } from '../../utils/auth';
import Header from './Header';
import Details from './modals/Details';
import FilterModal from './modals/FilterModal';
import SubmitEvent from './modals/SubmitEvent';
import Wallettableitem from '/components/prodattab/item/tabe-item';
import WallettableTitle from '/components/prodattab/item/tabe-title/index';
import WallettableTitletd from '/components/prodattab/item/tabe-title/tabtitletd';

export default function SellTokens() {
    const [data, setData] = useState([]);
    const [currentPage, setCurrenPage] = useState(1);
    const [total, setTotal] = useState(null);
    const [open, setOpen] = useState(false);
    const [isDetailsOpen, setIsDetailsOpen] = useState(false);
    const [isSellOpen, setIsSellOpen] = useState(false);
    const [filters, setFilters] = useState('');

    const { t } = useTranslation();

    useEffect(() => {
        fetch(
            `https://api.digital-investor.kz/api/secondary/get-all-requests?page=${currentPage}&per_page=8&type=buy${filters}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getToken(),
                    Accept: 'application/json',
                },
            }
        )
            .then((res) => res.json())
            .then(({ data }) => {
                setData(data?.requests_data?.data);
                setTotal(data?.total_count);
            });
    }, [currentPage, filters]);

    return (
        <>
            {!!open && <FilterModal close={() => setOpen(false)} setFilters={setFilters} />}
            {!!isDetailsOpen && <Details id={isDetailsOpen} close={() => setIsDetailsOpen(false)} />}
            {!!isSellOpen && <SubmitEvent id={isSellOpen} close={() => setIsSellOpen(false)} type="sell" />}
            <div className="tokens-tab tokens-tab2">
                <div className="tokens-tab-cont">
                    <Header callback={() => setOpen(true)} />
                    <div className="tokens-tab-cont-table">
                        <table className="prodattab">
                            <tbody>
                                <WallettableTitle>
                                    <WallettableTitletd tdtext={t('market_tab_2')} />
                                    <WallettableTitletd tdtext={t('market_tab_4')} />
                                    <WallettableTitletd tdtext={t('market_tab_6')} />
                                    <WallettableTitletd tdtext={t('market_tab_7')} />
                                    <WallettableTitletd tdtext={t('market_tab_8')} />
                                    <WallettableTitletd tdtext={''} />
                                    <WallettableTitletd tdtext={''} />
                                </WallettableTitle>
                                {!!data.length &&
                                    data.map((item, index) => (
                                        <Wallettableitem
                                            key={index}
                                            tokenp={item?.token_name}
                                            valyutp={item?.token_nominal_cost + item?.currency}
                                            tokens={item?.company_name}
                                            price={item?.percent_rate + '%'}
                                            dedline={item?.token_count}
                                            lastp={item?.token_cost + ' ' + item?.currency}
                                            lasts={'+0.05 ' + item?.currency}
                                            id={item?.users_token_id}
                                            idRequest={item?.request_id}
                                            openDetails={setIsDetailsOpen}
                                            openSell={setIsSellOpen}
                                        />
                                    ))}
                            </tbody>
                        </table>
                    </div>
                    <Pagination
                        showSizeChanger={false}
                        onChange={(e) => setCurrenPage(e)}
                        defaultCurrent={1}
                        current={currentPage}
                        total={total}
                    />
                </div>
            </div>
        </>
    );
}
