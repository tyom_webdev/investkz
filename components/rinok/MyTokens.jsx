import { Pagination } from 'antd';

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken } from '../../utils/auth';
import Header from './Header';
import FilterModal from './modals/FilterModal';
import Wallettableblock from '../prodattab/item/index';

/* eslint-disable */

export default function MyTokens() {
    const [data, setData] = useState([]);
    const [currentPage, setCurrenPage] = useState(1);
    const [total, setTotal] = useState(null);
    const [open, setOpen] = useState(false);
    const [filters, setFilters] = useState('');

    const { t } = useTranslation();

    useEffect(() => {
        fetch(
            `https://api.digital-investor.kz/api/secondary/get-user-tokens?page=${currentPage}&per_page=8${filters}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + getToken(),
                    Accept: 'application/json',
                },
            }
        )
            .then((res) => res.json())
            .then((res) => {
                setData(res?.data?.tokens_data?.data);
                setTotal(res?.data?.total_count);
            });
    }, [currentPage, filters]);

    return (
        <>
            {!!open && <FilterModal close={() => setOpen(false)} setFilters={setFilters} />}
            <div className="tokens-tab tokens-tab2">
                <div className="tokens-tab-cont">
                    <Header callback={() => setOpen(true)} />
                    {!!data && <Wallettableblock data={data} />}
                    <Pagination
                        showSizeChanger={false}
                        onChange={(e) => setCurrenPage(e)}
                        current={currentPage}
                        defaultPageSize={8}
                        pageSize={8}
                        total={total}
                    />
                </div>
            </div>
        </>
    );
}
