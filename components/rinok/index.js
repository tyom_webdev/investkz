import { Tabs } from 'antd';

import React from 'react';
import { useTranslation } from 'react-i18next';

import Rinoktable from '../rinoktab';
import BuyTokens from './BuyTokens';
import MyTokens from './MyTokens';
import SellTokens from './SellTokens';

const { TabPane } = Tabs;

const Rinok = () => {
    const { t } = useTranslation();

    return (
        <div className="wallet walletrin">
            <div className="wallet-cont">
                <Tabs defaultActiveKey="1">
                    <TabPane tab={t('market_nav1')} key="1">
                        <MyTokens />
                    </TabPane>
                    <TabPane tab={t('market_nav2')} key="2">
                        <Rinoktable />
                    </TabPane>
                    <TabPane tab={t('market_nav3')} key="3">
                        <SellTokens />
                    </TabPane>
                    <TabPane tab={t('market_nav4')} key="4">
                        <BuyTokens />
                    </TabPane>
                </Tabs>
            </div>
        </div>
    );
};

export default Rinok;
