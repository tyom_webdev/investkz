import React from 'react';
import { useTranslation } from 'react-i18next';

/* eslint-disable */

export default function Header({ callback }) {
    const { t } = useTranslation();

    return (
        <div className="tokens-tab-cont-title">
            <div className="tokens-tab-cont-title-left">
                <h1>{t('market_header')}</h1>
                <a href="tarif">{t('menu_docs')}</a>
            </div>
            <div className="tokens-tab-cont-title-right">
                <div onClick={callback} className="tokens-tab-cont-title-right-item">
                    <img src="/img/sacacsasc.png" />
                    <p>{t('show_filters')}</p>
                </div>
            </div>
        </div>
    );
}
