import PropTypes from 'prop-types';

import React from 'react';
import { useTranslation } from 'react-i18next';

function Modal({ data, onClose }) {
    const { t } = useTranslation();

    return (
        <div className="newsmodal" style={{ display: 'block' }}>
            <div className="newsmodal-cont">
                <button onClick={onClose} className="clousmodal"></button>
                <div className="newsmodal-cont-top">
                    <div className="newsmodal-cont-top-box">
                        {!!data?.image && (
                            <img src={data?.image} alt={data?.title} draggable="false" style={{ width: '100%' }} />
                        )}
                        <p className="title">
                            {data?.title}
                            <span>{data?.created_at.split('T')[0]}</span>
                        </p>
                        <p className="info">{data?.description}</p>
                        {!!data?.tags && (
                            <div className="heshtag">
                                {data?.tags.map((tag, index) => (
                                    <a key={index} href="/">
                                        {tag}
                                    </a>
                                ))}
                            </div>
                        )}
                        <div className="infoflex">
                            <div className="infoflex-item">
                                <span>+19%</span>
                                <a href="/">{t('news_modal_text1')}</a>
                                <p>{t('news_modal_text1_1')}</p>
                            </div>
                            <div className="infoflex-item">
                                <span>+45%</span>
                                <a href="/">{t('news_modal_text2')}</a>
                                <p>{t('news_modal_text2_2')}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="newsmodal-cont-bottom">
                    <div className="newsmodal-cont-bottom-box">
                        <button onClick={onClose}>{t('btn_invest')}</button>
                        <div className="newsmodal-cont-bottom-box-right">
                            <p>{t('btn_news')}</p>
                            <div className="newsmodal-cont-bottom-box-right-flex" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

Modal.propTypes = {
    data: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default Modal;
