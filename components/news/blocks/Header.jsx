import PropTypes from 'prop-types';

import React from 'react';
import { useTranslation } from 'react-i18next';

export default function Header({ children }) {
    const { t } = useTranslation();

    return (
        <div className="news-filter">
            <div className="news-cont">
                <div className="news-cont-title">
                    <h1>{t('news')}</h1>
                    <p>{t('news_info')}</p>
                </div>
                <div className="news-cont-filter">
                    <div className="news-cont-filter-right" style={{ maxWidth: 'none' }}>
                        {children}
                    </div>
                </div>
            </div>
        </div>
    );
}

Header.propTypes = {
    children: PropTypes.node.isRequired,
};
