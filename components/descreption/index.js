import React from 'react';
import { useTranslation } from 'react-i18next';

const DescriptionHome = () => {
    const { t } = useTranslation();

    return (
        <div className="description-home">
            <div className="container">
                <div className="description-home_contentn">
                    <div className="description-home_contentn-item">
                        <img src="/img/shield.png" alt="" />
                        <p>{t('how_start1')}</p>
                    </div>

                    <div className="description-home_contentn-item">
                        <img src="/img/shield.png" alt="" />
                        <p>{t('how_start2')}</p>
                    </div>

                    <div className="description-home_contentn-item">
                        <img src="/img/shield.png" alt="" />
                        <p>{t('how_start3')}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DescriptionHome;
