import { Button, Input } from 'antd';

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken } from '../../utils/auth';
import Reftable from '../reftable';
import Refproitem from './item/index';

const Refpro = () => {
    const [data, setData] = useState(null);
    const [page, setPage] = useState(1);
    const [filters, setFilters] = useState('');

    const { t } = useTranslation();

    useEffect(() => {
        fetch(`https://api.digital-investor.kz/api/ref?page=${page}&per_page=12&${filters}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getToken(),
                Accept: 'application/json',
            },
        })
            .then((res) => res.json())
            .then((res) => {
                setData(res?.data);
            });
    }, [page, filters]);
    
    return (
        <div className="refpro tokens-tab wallet">
            <div className="refpro-cont">
                <h2>{t('referal_program')}</h2>
                <div className="refpro-cont-flex">
                    <Refproitem title={t('referals')} desc={t('all_referrals')} number={data?.ref_count} />
                    <Refproitem title={t('deals')} desc={t('all_deals')} number={data?.deal_count} />
                    <Refproitem title={t('bonuses')} desc={''} number={''} amounts={data?.amount} />
                </div>
                <h2 style={{ marginTop: '25px' }}>{t('referal_link')}</h2>
                <div
                    style={{
                        width: '100%',
                        marginTop: '25px',
                        display: 'flex',
                        maxWidth: '600px',
                    }}
                >
                    <Input readOnly value={data?.ref_link} size="small" />
                    <Button
                        style={{ marginLeft: '10px' }}
                        type="button"
                        onClick={() => navigator.clipboard.writeText(data?.ref_link)}
                    >
                        {t('copy')}
                    </Button>
                </div>
                <div className="walletstoritable">
                    <Reftable data={data?.history} page={page} setPage={setPage} setFilters={setFilters} />
                </div>
            </div>
        </div>
    );
};
export default Refpro;
