import { Tabs } from 'antd';
import { v4 as uuidv4 } from 'uuid';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import Button from '../../uikit/button';
import Token from '../../uikit/token';
import { getToken } from '../../utils/auth';
import Toknewmodal from '../tokenmodalnev';
import Zayavmodal from '../zayavmodal';

const { TabPane } = Tabs;

const TokensPage = () => {
    const [tab, setTab] = useState('1');
    const [user_token_id, set_user_token_id] = useState(null);
    const [earlyRedemptionData, setEarlyRedemptionData] = useState();
    const [selectedTokenId, setSelectedTokenId] = useState(null);

    const { t } = useTranslation();

    useEffect(() => {
        const url =
            tab === '1'
                ? 'https://api.digital-investor.kz/api/get-early-redemption'
                : 'https://api.digital-investor.kz/api/get-user-redemptions';

        fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getToken(),
            },
        })
            .then((res) => res.json())
            .then(({ data }) => {
                setEarlyRedemptionData(data);
            });
    }, [tab]);

    const handleTabChange = (e) => {
        setTab(e);
    };

    function openzayavmodal(d) {
        document.getElementById('zayavmodal').style.display = 'block';
        setSelectedTokenId(d);
    }

    const opentoknewmodal = (d) => {
        document.getElementById('tokennevmodal').style.display = 'block';
        setSelectedTokenId(d);
    };

    return (
        <div className="tokens-wrapper">
            <Toknewmodal token_id={selectedTokenId} />
            <Zayavmodal token_id={selectedTokenId} user_token_id={user_token_id} />
            <div className="container">
                <Tabs defaultActiveKey="1" onChange={handleTabChange}>
                    <TabPane tab={t('early_tokens')} key="1">
                        <h4 className="tokens__heading">{t('early_tokens')}</h4>
                        <a href="tarif" className="tokens__infolink">
                            {t('menu_docs')}
                        </a>
                        <div className="tokensBlock">
                            {earlyRedemptionData &&
                                new Array(Math.ceil(earlyRedemptionData.length / 4)).fill(0).map((_, i) => {
                                    return (
                                        <div key={uuidv4()} className="tokensBlock-row">
                                            {earlyRedemptionData.slice(i * 4, (i + 1) * 4).map((el) => {
                                                return (
                                                    <Token
                                                        key={uuidv4()}
                                                        path={el.company_logo}
                                                        name={el?.token_name}
                                                        production={el?.company_name}
                                                        saled={el?.buy_tokens_count}
                                                        torepayment={el?.my_repayment_tokens_count}
                                                        btn={
                                                            <Button
                                                                onClick={() => opentoknewmodal(el.token_id)}
                                                                color="submit"
                                                            >
                                                                {t('More')}
                                                            </Button>
                                                        }
                                                        btn2={
                                                            <Button
                                                                onClick={() => {
                                                                    openzayavmodal(el.token_id);
                                                                    set_user_token_id(el?.user_token_id);
                                                                }}
                                                                color="submit"
                                                            >
                                                                {t('get_request')}
                                                            </Button>
                                                        }
                                                    />
                                                );
                                            })}
                                        </div>
                                    );
                                })}
                        </div>
                    </TabPane>
                    <TabPane tab={t('my_early_requests')} key="2">
                        <h4 className="tokens__heading">{t('my_early_requests')}</h4>
                        <a href="tarif" className="tokens__infolink">
                            {t('menu_docs')}
                        </a>
                        <div className="tokensBlock">
                            {earlyRedemptionData &&
                                new Array(Math.ceil(earlyRedemptionData.length / 4)).fill(0).map((_, i) => {
                                    return (
                                        <div key={uuidv4()} className="tokensBlock-row">
                                            {earlyRedemptionData.slice(i * 4, (i + 1) * 4).map((el) => {
                                                return (
                                                    <Token
                                                        key={uuidv4()}
                                                        path={el?.company_logo}
                                                        name={el?.token_name}
                                                        production={el?.company_name}
                                                        saled={el?.buy_tokens_count}
                                                        torepayment={el?.token_count}
                                                        btn={
                                                            <Button
                                                                onClick={() => opentoknewmodal(el.token_id)}
                                                                color="submit"
                                                            >
                                                                {t('More')}
                                                            </Button>
                                                        }
                                                    />
                                                );
                                            })}
                                        </div>
                                    );
                                })}
                        </div>
                    </TabPane>
                </Tabs>
            </div>
        </div>
    );
};

export default TokensPage;
