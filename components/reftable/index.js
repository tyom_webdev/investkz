import { Pagination } from 'antd';

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import Filters from './modals/Filters';
import Reftableblock from './item/index';

/* eslint-disable */

const Reftable = ({ data, page, setPage, setFilters }) => {
    const [isFiltersOpen, setIsFiltersOpen] = useState(false);

    const { t } = useTranslation();

    return (
        <>
            {!!isFiltersOpen && <Filters setFilters={setFilters} onClose={() => setIsFiltersOpen(false)} />}
            <div className="tokens-tab" style={{ paddingTop: '0px!important' }}>
                <div className="tokens-tab-cont">
                    <div className="tokens-tab-cont-title">
                        <div className="tokens-tab-cont-title-left">
                            <h1>{t('accrual_history')}</h1>
                            <a href="tarif">{t('menu_docs')}</a>
                        </div>
                        <div
                            className="tokens-tab-cont-title-right tokens-tab-cont-title-rightmin"
                            style={{ justifyContent: 'flex-end' }}
                        >
                            <div
                                className="tokens-tab-cont-title-right-item"
                                style={{ cursor: 'pointer' }}
                                onClick={() => setIsFiltersOpen(true)}
                            >
                                <img src="/img/sacacsasc.png" />
                                <p>{t('show_filters')}</p>
                            </div>
                        </div>
                    </div>
                    {!!data && (
                        <React.Fragment>
                            <Reftableblock data={data?.data} />
                            <Pagination
                                defaultPageSize={data?.per_page}
                                pageSize={data?.per_page}
                                onChange={(e) => setPage(e)}
                                current={page}
                                defaultCurrent={1}
                                total={data?.total}
                            />
                        </React.Fragment>
                    )}
                </div>
            </div>
        </>
    );
};

export default Reftable;
