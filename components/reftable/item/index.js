import React from 'react';
import { useTranslation } from 'react-i18next';

import Reftableitem from './tabe-item';
import WallettableTitle from './tabe-title/index';
import ReftableTitletd from './tabe-title/tabtitletd';

const Reftableblock = ({ data }) => {
    const { t } = useTranslation();

    return (
        <div className="tokens-tab-cont-table">
            <table>
                <tbody>
                    <WallettableTitle>
                        <ReftableTitletd tdtext={t('operation_type')} />
                        <ReftableTitletd tdtext={t('amount')} />
                        <ReftableTitletd tdtext={t('currency')} />
                        <ReftableTitletd tdtext={t('date_time')} />
                    </WallettableTitle>
                    {!!data &&
                        data.map((item) => (
                            <Reftableitem
                                key={item?.id}
                                tokenp={item?.operation_type}
                                valyutp={item?.amount}
                                price={item?.currency.toUpperCase() + '.sc'}
                                dedline={item?.created_at.split('T')[0]}
                            />
                        ))}
                </tbody>
            </table>
        </div>
    );
};

export default Reftableblock;
