import React from 'react';

const WallettableTitle = ({ children }) => {
    return <tr className="tantitletr">{children}</tr>;
};

export default WallettableTitle;
