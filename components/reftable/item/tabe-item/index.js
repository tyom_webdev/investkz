import React from 'react';

const Reftableitem = ({ tokenp, tokens, valyutp, valyuts, price, dedline }) => {
    return (
        <tr className="tabitemtr walletitemtr walletitemtr4">
            <td>
                <p>{tokenp}</p>
                <span>{tokens}</span>
            </td>
            <td>
                <p>{valyutp}</p>
                <span>{valyuts}</span>
            </td>
            <td>
                <p>{price}</p>
            </td>
            <td>
                <p>{dedline}</p>
            </td>
        </tr>
    );
};

export default Reftableitem;
