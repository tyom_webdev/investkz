import { Carousel } from 'antd';
import 'antd/dist/antd.css';

import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

const buttonStyles = {
    outline: 'none',
    border: 'none',
    background: 'transparent',
    cursor: 'pointer',
};

class CarouselComponent extends Component {
    constructor(props) {
        super(props);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.carousel = React.createRef();
        this.state = { div: '' };
        this.popUpFunc = this.popUpFunc.bind(this);
        this.popUpNoned = this.popUpNoned.bind(this);
    }

    next() {
        this.carousel.next();
    }

    previous() {
        this.carousel.prev();
    }

    popUpNoned(e) {
        if (e.target.src === undefined) {
            this.setState({
                div: '',
            });
        }
    }

    popUpFunc(imgSrc, arg) {
        if (arg == 'display-block') {
            this.setState({
                div: (
                    <button
                        onClick={this.popUpNoned}
                        className="popUpDiv"
                        style={{
                            width: '100%',
                            height: '100vh',
                            position: 'absolute',
                            zIndex: '9999',
                            background: 'rgba(194, 193, 193, 0.562) !important',
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            ...buttonStyles,
                        }}
                    >
                        <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                            <div></div>
                            <button onClick={this.popUpFunc} style={buttonStyles}>
                                <img src="/img/blue-number.png" alt="" />
                                <p
                                    style={{
                                        position: 'absolute',
                                        fontSize: '23px',
                                        marginTop: '-50px',
                                        marginLeft: '10px',
                                        cursor: 'pointer',
                                    }}
                                >
                                    X
                                </p>
                            </button>
                        </div>
                        <div>
                            <img style={{ width: '800px' }} src={`${imgSrc}`} alt="" />
                        </div>
                        <div></div>
                    </button>
                ),
            });
        } else {
            this.setState({
                div: '',
            });
        }
    }
    render() {
        const props = {
            dots: true,
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
        };

        const { t } = this.props;

        return (
            <div className="how-home">
                {this.state.div}
                <div className="container">
                    <div className="how-home_content">
                        <div className="how-home_content-header">
                            <h2>{t('how_start_tl')}</h2>

                            <div className="actions-how">
                                <button onClick={this.previous} style={buttonStyles} className="actions-how_item">
                                    <img src="/img/acsascas.png" alt="" draggable="false" />
                                </button>
                                <button
                                    onClick={this.next}
                                    className="actions-how_item"
                                    style={{ ...buttonStyles, background: '#49B193 !important' }}
                                >
                                    <img src="/img/ascaascasqw.png" alt="" draggable="false" />
                                </button>
                            </div>
                        </div>
                        <Carousel autoplay loop ref={(node) => (this.carousel = node)} {...props}>
                            <div className="how-home_content-slides">
                                <button
                                    onClick={() => this.popUpFunc('/img/Capture.png', 'display-block')}
                                    className="how-home_content-slides_slide"
                                    style={buttonStyles}
                                >
                                    <div className="blue-number">
                                        <img src="/img/blue-number.png" alt="" draggable="false" />
                                        <p>01</p>
                                    </div>

                                    <h6>{t('how_start_slide1_tl')}</h6>
                                    <p>{t('how_start_slide1_desc')}</p>

                                    <img src="/img/Capture.png" alt="" draggable="false" />
                                </button>

                                <button
                                    onClick={() => this.popUpFunc('/img/Captascure.png', 'display-block')}
                                    className="how-home_content-slides_slide"
                                    style={buttonStyles}
                                >
                                    <div className="blue-number">
                                        <img src="/img/blue-number.png" alt="" draggable="false" />
                                        <p>02</p>
                                    </div>
                                    <h6>{t('how_start_slide2_tl')}</h6>
                                    <p>{t('how_start_slide2_desc')}</p>

                                    <img src="/img/Captascure.png" alt="" draggable="false" />
                                </button>
                            </div>
                            <div className="how-home_content-slides">
                                <button
                                    onClick={() => this.popUpFunc('/img/Capasdture.png', 'display-block')}
                                    className="how-home_content-slides_slide"
                                    style={buttonStyles}
                                >
                                    <div className="blue-number">
                                        <img src="/img/blue-number.png" alt="" draggable="false" />
                                        <p>03</p>
                                    </div>

                                    <h6>{t('how_start_slide3_tl')}</h6>
                                    <p>{t('how_start_slide3_desc')}</p>

                                    <img src="/img/Capasdture.png" alt="" draggable="false" />
                                </button>

                                <button
                                    onClick={() => this.popUpFunc('/img/Captur5455e.png', 'display-block')}
                                    className="how-home_content-slides_slide"
                                    style={buttonStyles}
                                >
                                    <div className="blue-number">
                                        <img src="/img/blue-number.png" alt="" draggable="false" />
                                        <p>04</p>
                                    </div>
                                    <h6>{t('how_start_slide4_tl')}</h6>
                                    <p>{t('how_start_slide4_desc')}</p>

                                    <img src="/img/Captur5455e.png" alt="" draggable="false" />
                                </button>
                            </div>
                        </Carousel>
                    </div>
                </div>
            </div>
        );
    }
}

export default withTranslation()(CarouselComponent);
