import Dropdown from 'antd/lib/dropdown';
import PropTypes from 'prop-types';

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

function DateModal({ close, setDates }) {
    const [start, setStart] = useState(null);
    const [end, setEnd] = useState(null);

    const { t } = useTranslation();

    const handleResetFilters = () => {
        setDates({
            date_from: '',
            date_to: '',
        });

        setTimeout(() => close(), 500);
    };

    const handleSubmitFilters = () => {
        setDates({
            date_from: start,
            date_to: end,
        });

        setTimeout(() => close(), 500);
    };

    return (
        <div className="filter" style={{ display: 'block' }}>
            <div className="filter-cont">
                <button onClick={close} className="clousmodal"></button>
                <div className="filter-cont-title">
                    <div className="filter-cont-title-box">
                        <div className="filter-cont-title-box-flex">
                            <h3>{t('select_dating')}</h3>
                            <span>{t('accordance')}</span>
                        </div>
                        <p>{t('modal_text')}</p>
                    </div>
                </div>
                <div className="filter-cont-form">
                    <div className="filter-cont-form-box">
                        <div className="filter-cont-form-box-top">
                            <label htmlFor="start">
                                {t('start_date')}
                                <Dropdown overlay={''}>
                                    <input
                                        id="start"
                                        type="date"
                                        onChange={(e) => setStart(e.target.valueAsNumber)}
                                        placeholder="27.02.2021"
                                        required
                                    />
                                </Dropdown>
                            </label>
                            <label htmlFor="end">
                                {t('end_date')}
                                <Dropdown overlay={''}>
                                    <input
                                        id="end"
                                        type="date"
                                        onChange={(e) => setEnd(e.target.valueAsNumber)}
                                        placeholder="27.02.2021"
                                        required
                                    />
                                </Dropdown>
                            </label>
                        </div>
                        <div className="filter-cont-form-box-bottom">
                            <label>
                                <input type="button" value={t('save')} onClick={handleSubmitFilters} />
                            </label>
                            <label>
                                <input type="button" value={t('clear')} onClick={handleResetFilters} />
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

DateModal.propTypes = {
    close: PropTypes.func.isRequired,
    setDates: PropTypes.func.isRequired,
};

export default DateModal;
