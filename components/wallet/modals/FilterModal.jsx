import { Menu } from 'antd';
import Dropdown from 'antd/lib/dropdown';
import PropTypes from 'prop-types';

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

function FilterModal({ close, setFilters }) {
    const [currency, setCurrency] = useState('');
    const [kind, setKind] = useState('');

    const { t } = useTranslation();

    const currencies = [
        { v: '', d: t('all') },
        { v: 'kzt_sc', d: 'KZT' },
        { v: 'rub_sc', d: 'RUB' },
        { v: 'usd_sc', d: 'USD' },
        { v: 'euro_sc', d: 'EUR' },
    ];

    const kinds = [
        { v: '', d: t('all_types') },
        { v: 1, d: t('buy_user_token') },
        { v: 2, d: t('get_discount') },
        { v: 3, d: t('withdraw_wallet') },
        { v: 4, d: t('fill_wallet') },
        { v: 5, d: t('early_red_token') },
        { v: 6, d: t('sale_token_sec_market') },
        { v: 7, d: t('buy_token_sec_market') },
    ];

    const menuCurrency = (
        <Menu>
            {currencies
                .filter((i) => i.v !== currency)
                .map((i, index) => (
                    <Menu.Item key={index} onClick={() => setCurrency(i.v)}>
                        <span>{i.d}</span>
                    </Menu.Item>
                ))}
        </Menu>
    );

    const menuKind = (
        <Menu>
            {kinds
                .filter((i) => i.v !== kind)
                .map((i, index) => (
                    <Menu.Item key={index} onClick={() => setKind(i.v)}>
                        <span>{i.d}</span>
                    </Menu.Item>
                ))}
        </Menu>
    );

    const handleResetFilters = () => {
        setFilters({
            by_currency: '',
            by_operation: '',
        });

        setTimeout(() => close(), 500);
    };

    const handleSubmitFilters = () => {
        setFilters({
            by_currency: currency,
            by_operation: kind,
        });

        setTimeout(() => close(), 500);
    };

    return (
        <div className="filter" style={{ display: 'block' }}>
            <div className="filter-cont">
                <button onClick={close} className="clousmodal"></button>
                <div className="filter-cont-title">
                    <div className="filter-cont-title-box">
                        <div className="filter-cont-title-box-flex">
                            <h3>{t('filters')}</h3>
                            <span>{t('accordance')}</span>
                        </div>
                        <p>
                            {t('modal_text')}
                        </p>
                    </div>
                </div>
                <div className="filter-cont-form">
                    <div className="filter-cont-form-box">
                        <div className="filter-cont-form-box-top">
                            <label htmlFor='UNIQUE_TEST_ID123456789'>
                                {t('select_currency')}
                                <Dropdown overlay={menuCurrency}>
                                    <div>
                                        {currencies
                                            .filter((i) => i.v === currency)
                                            .map((i, index) => (
                                                <span key={index}>{i.d}</span>
                                            ))}
                                        <img src="/img/arrow-select.png" alt="" />
                                    </div>
                                </Dropdown>
                            </label>
                            <label htmlFor='UNIQUE_TEST_ID1234567890'>
                                {t('operation_type')}
                                <Dropdown overlay={menuKind}>
                                    <div>
                                        {kinds
                                            .filter((i) => i.v === kind)
                                            .map((i, index) => (
                                                <span key={index}>{i.d}</span>
                                            ))}
                                        <img src="/img/arrow-select.png" alt="" />
                                    </div>
                                </Dropdown>
                            </label>
                        </div>
                        <div className="filter-cont-form-box-bottom">
                            <label>
                                <input type="button" value={t('save')} onClick={handleSubmitFilters} />
                            </label>
                            <label>
                                <input type="button" value={t('clear')} onClick={handleResetFilters} />
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

FilterModal.propTypes = {
    close: PropTypes.func.isRequired,
    setFilters: PropTypes.func.isRequired,
};

export default FilterModal;
