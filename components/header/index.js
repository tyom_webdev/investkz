import React from 'react';
import { useTranslation } from 'react-i18next';

import Button from '../../uikit/button';
import { isAuthorizated } from '../../utils/auth';

const HeaderHome = () => {
    const { t } = useTranslation();

    function handleToggle() {
        if (isAuthorizated()) {
            window.location.href = `${window.location.origin}/usertokens`;
        } else {
            document.getElementById('regmodal').style.display = 'block';
        }
    }

    return (
        <div className="header-home">
            <div className="container">
                <div className="header-home_content">
                    <h1>
                        <span>{t('top_tl1')}</span> <br />
                        {t('top_tl2')}
                    </h1>
                    <p>{t('top_tl3')}</p>

                    <Button onClick={handleToggle} color={'green'}>
                        {t('btn_invest')}
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default HeaderHome;
