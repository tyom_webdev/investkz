import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken } from '../../utils/auth';

const Reviewmodal = () => {
    const [reviewText, setReviewText] = useState('');

    const { t } = useTranslation();

    const submit = (e) => {
        e.preventDefault();
        fetch('https://api.digital-investor.kz/api/add-reviews', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + getToken(),
                Accept: 'application/json',
            },
            body: JSON.stringify({
                review: reviewText,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                if (data.status === 200) {
                    close();
                } else {
                    alert(data.message);
                }
            });
    };

    function close() {
        document.getElementById('reviewmodal').style.display = 'none';
    }

    return (
        <div className="reviewmodal" id="reviewmodal">
            <div className="reviewmodal-cont">
                <button onClick={close} className="clousmodal"></button>
                <div className="reviewmodal-cont-title">
                    <div className="reviewmodal-cont-title-box">
                        <h3>{t('review_modal_title')}</h3>
                    </div>
                </div>
                <div className="reviewmodal-cont-form">
                    <form className="reviewmodal-cont-form-box">
                        <div className="reviewmodal-cont-form-box-bottom">
                            <div className="reviewmodal-cont-form-box-bottom-left">
                                <label>
                                    <textarea
                                        onChange={(e) => setReviewText(e.target.value)}
                                        type="text"
                                        placeholder={t('review_modal_placeholder')}
                                    />
                                </label>
                            </div>
                            <div className="reviewmodal-cont-form-box-bottom-right">
                                <label>
                                    <input onClick={submit} type="submit" value={t('send')}/>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Reviewmodal;
