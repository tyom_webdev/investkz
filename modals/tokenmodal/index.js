import { Menu } from 'antd';
import Dropdown from 'antd/lib/dropdown';

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { getToken, logout } from '../../utils/auth';

const currencies = [
    { v: 'euro_sc', display: 'EUR.sc' },
    { v: 'kzt_sc', display: 'KZT.sc' },
    { v: 'rub_sc', display: 'RUB.sc' },
    { v: 'usd_sc', display: 'USD.sc' },
];

function cloustokenmodalmodal() {
    document.getElementById('tokenmodal').style.display = 'none';
}

const Tokenmodal = () => {
    const [total, setTotal] = useState('');
    const [currency, setCurrency] = useState({
        v: currencies[0].v,
        display: currencies[0].display,
    });
    const [response, setResponse] = useState({
        type: null,
        message: null,
    });

    const { t } = useTranslation();

    const handleSubmit = (e) => {
        e.preventDefault();

        fetch('https://api.digital-investor.kz/api/check-user-anket', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Bearer ' + getToken(),
            },
        })
            .then((res) => res.json())
            .then((res) => {
                if (!res.data.has_anket) {
                    setResponse({
                        type: 'error',
                        message: t('empty_anket'),
                    });
                } else {
                    const data = {
                        output_amount: total,
                        currency: currency.v,
                    };

                    fetch('https://api.digital-investor.kz/api/decrease-user-wallet', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            Authorization: 'Bearer ' + getToken(),
                        },
                        body: JSON.stringify(data),
                    }).then((res) => {
                        if (res.status === 401) {
                            logout();
                        }
                        setResponse({
                            type: 'success',
                            message: t('withdraw_success'),
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                        return res.json();
                    });
                }
            });
    };

    const menuCurrency = (
        <Menu>
            {currencies.map((item, index) => (
                <Menu.Item key={index} onClick={() => setCurrency({ v: item.v, display: item.display })}>
                    <span>{item.display}</span>
                </Menu.Item>
            ))}
        </Menu>
    );

    return (
        <div className="tokenmodal" id="tokenmodal">
            <div className="tokenmodal-cont">
                <button onClick={cloustokenmodalmodal} className="clousmodal"></button>
                <div className="tokenmodal-cont-title">
                    <div className="tokenmodal-cont-title-box">
                        <h3>{t('withdraw_money')}</h3>
                        <p>{t('withdraw_message')}</p>
                    </div>
                </div>
                <div className="tokenmodal-cont-form">
                    <form onSubmit={handleSubmit} className="tokenmodal-cont-form-box">
                        <div className="tokenmodal-cont-form-box-top">
                            <label htmlFor="TEST_UNIQUE_GEN">
                                {t('select_currency')}
                                <Dropdown overlay={menuCurrency}>
                                    <div>
                                        <span>{currency.display}</span>
                                        <img src="/img/arrow-select.png" alt="" />
                                    </div>
                                </Dropdown>
                            </label>
                            <label>
                                {t('number_count')}
                                <input type="text" placeholder="2542547745771554777" />
                            </label>
                        </div>
                        <div className="tokenmodal-cont-form-box-bottom">
                            <div className="tokenmodal-cont-form-box-bottom-left">
                                <label>
                                    {t('number_count_bank')}
                                    <input type="text" placeholder="758585627" />
                                </label>
                                <label>
                                    {t('tab_num')}
                                    <input
                                        type="text"
                                        onChange={(e) => setTotal(e.target.value)}
                                        placeholder="7000"
                                        required
                                    />
                                </label>
                            </div>
                            <div className="tokenmodal-cont-form-box-bottom-right">
                                {!!response.message && (
                                    <span
                                        style={{
                                            fontWeight: 500,
                                            fontSize: 17,
                                            lineHeight: '25px',
                                            color: response.type === 'error' ? 'red' : '#14906c',
                                        }}
                                    >
                                        {response.message}
                                    </span>
                                )}
                                <label>
                                    <input type="submit" value={t('withdraw_button')} style={{ cursor: 'pointer' }} />
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Tokenmodal;
